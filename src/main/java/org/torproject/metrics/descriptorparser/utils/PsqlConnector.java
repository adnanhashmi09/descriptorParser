package org.torproject.metrics.descriptorparser.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class PsqlConnector {

  private static final Logger logger = LoggerFactory.getLogger(
      PsqlConnector.class);

  private Properties loadConfiguration(String fileName) {
    Properties prop = new Properties();
    try (FileInputStream fis = new FileInputStream(fileName)) {
      prop.load(fis);
    } catch (IOException ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }
    return prop;
  }

  /**
   * Connects to a DB given connection properties passed via a
   * configuration file.
   */
  public Connection connect(String configurationFile) {
    Connection conn = null;

    Properties prop = this.loadConfiguration(configurationFile);
    String host = prop.getProperty("HOST");
    String port = prop.getProperty("PORT");
    String db = prop.getProperty("DATABASE");
    String user = prop.getProperty("USERNAME");
    String password = prop.getProperty("PASSWORD");

    String url = "jdbc:postgresql://" + host + ":" + port + "/" + db;

    try {
      conn = DriverManager.getConnection(url, user, password);
      logger.info("Connected to the PostgreSQL server successfully.");
    } catch (SQLException ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }

    return conn;
  }
}
