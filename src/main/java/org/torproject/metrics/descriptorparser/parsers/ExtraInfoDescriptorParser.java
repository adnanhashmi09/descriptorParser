package org.torproject.metrics.descriptorparser.parsers;

import org.torproject.descriptor.BandwidthHistory;
import org.torproject.descriptor.BridgeExtraInfoDescriptor;
import org.torproject.descriptor.Descriptor;
import org.torproject.descriptor.DescriptorReader;
import org.torproject.descriptor.DescriptorSourceFactory;
import org.torproject.descriptor.ExtraInfoDescriptor;
import org.torproject.descriptor.RelayExtraInfoDescriptor;
import org.torproject.metrics.descriptorparser.utils.DescriptorUtils;
import org.torproject.metrics.descriptorparser.utils.Gauge;
import org.torproject.metrics.descriptorparser.utils.OpenMetricsWriter;

import io.prometheus.client.CollectorRegistry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

public class ExtraInfoDescriptorParser {

  private boolean isBridge = true;

  private static final String INSERT_EXTRA_INFO_SQL
      = "INSERT INTO extra_info_descriptor"
      + " (is_bridge, published, nickname, fingerprint,"
      + " digest_sha1_hex, identity_ed25519,"
      + " master_key_ed25519, read_history,"
      + " write_history, ipv6_read_history,"
      + " ipv6_write_history, geoip_db_digest,"
      + " geoip6_db_digest, geoip_start_time,"
      + " geoip_client_origins, bridge_stats_end,"
      + " bridge_stats_inverval, bridge_ips,"
      + " bridge_ip_versions, bridge_ip_transports,"
      + " dirreq_stats_end, dirreq_stats_interval,"
      + " dirreq_v2_ips, dirreq_v3_ips,"
      + " dirreq_v2_reqs, dirreq_v3_reqs, dirreq_v2_share, dirreq_v3_share,"
      + " dirreq_v2_resp, dirreq_v3_resp,"
      + " dirreq_v2_direct_dl, dirreq_v3_direct_dl,"
      + " dirreq_v2_tunneled_dl, dirreq_v3_tunneled_dl,"
      + " dirreq_read_history, dirreq_write_history,"
      + " entry_stats_end, entry_stats_interval,"
      + " entry_ips, cell_stats_end,"
      + " cell_stats_interval, cell_processed_cells,"
      + " cell_queued_cells, cell_time_in_queue,"
      + " cell_circuits_per_decile, conn_bi_direct_timestamp,"
      + " conn_bi_direct_interval, conn_bi_direct_below,"
      + " conn_bi_direct_read, conn_bi_direct_write,"
      + " conn_bi_direct_both, ipv6_conn_bi_direct_timestamp,"
      + " ipv6_conn_bi_direct_interval, ipv6_conn_bi_direct_below,"
      + " ipv6_conn_bi_direct_read, ipv6_conn_bi_direct_write,"
      + " ipv6_conn_bi_direct_both, exit_stats_end,"
      + " exit_stats_inverval, exit_kibibytes_written,"
      + " exit_kibibytes_read, exit_streams_opened,"
      + " hidserv_stats_end, hidserv_stats_interval,"
      + " hidserv_v3_stats_end, hidserv_v3_stats_interval,"
      + " hidserv_rend_relayed_cells_value, hidserv_rend_relayed_cells,"
      + " hidserv_rend_v3_relayed_cells_value, hidserv_rend_v3_relayed_cells,"
      + " hidserv_dir_onions_seen_value, hidserv_dir_onions_seen,"
      + " hidserv_dir_v3_onions_seen_value, hidserv_dir_v3_onions_seen,"
      + " transports, padding_counts_timestamp,"
      + " padding_counts_interval, padding_counts, overload_ratelimits_version,"
      + " overload_ratelimits_timestamp,"
      + " overload_ratelimits_ratelimit, overload_ratelimits_burstlimit,"
      + " overload_ratelimits_readcount, overload_ratelimits_write_count,"
      + " overload_fd_exhausted_version, overload_fd_exhausted_timestamp,"
      + " router_sig_ed25519, router_signature, header) VALUES "
      + "(?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?, ?"
      + ") ON CONFLICT DO NOTHING;";

  private static final Logger logger = LoggerFactory.getLogger(
      ExtraInfoDescriptorParser.class);

  private static CollectorRegistry registry = new CollectorRegistry();
  private static OpenMetricsWriter opWriter = new OpenMetricsWriter();

  private static final long ONE_HOUR_MILLIS = 60L * 60L * 1000L;

  private static final long ONE_DAY_MILLIS = 24L * ONE_HOUR_MILLIS;

  private static final long ONE_WEEK_MILLIS = 7L * ONE_DAY_MILLIS;

  private static Gauge readBwGauge = Gauge.build()
      .name("read_bandwidth_history")
      .help("How much bandwidth the OR has used recently. Usage is divided "
          + "into intervals of NSEC seconds.")
      .labelNames("fingerprint", "nickname", "node").register(registry);

  private static Gauge writeBwGauge = Gauge.build()
      .name("write_bandwidth_history")
      .help("How much bandwidth the OR has used recently. Usage is divided "
          + "into intervals of NSEC seconds.")
      .labelNames("fingerprint", "nickname", "node").register(registry);

  private static Gauge ipv6ReadBwGauge = Gauge.build()
      .name("ipv6_read_bandwidth_history")
      .help("How much bandwidth the OR has used recently on ipv6 "
          + "connections. Usage is divided into intervals of NSEC seconds.")
      .labelNames("fingerprint", "nickname", "node").register(registry);

  private static Gauge ipv6WriteBwGauge = Gauge.build()
      .name("ipv6_write_bandwidth_history")
      .help("How much bandwidth the OR has used recently on ipv6 "
          + "connections. Usage is divided into intervals of NSEC seconds.")
      .labelNames("fingerprint", "nickname", "node").register(registry);

  private static Gauge dirreqReadBwGauge = Gauge.build()
      .name("dirreq_read_bandwidth_history")
      .help("Declare how much bandwidth the OR has spent on answering "
          + "directory requests. Usage is divided into intervals of NSEC "
          + "seconds.")
      .labelNames("fingerprint", "nickname", "node").register(registry);

  private static Gauge dirreqWriteBwGauge = Gauge.build()
      .name("dirreq_write_bandwidth_history")
      .help("Declare how much bandwidth the OR has spent on answering "
          + "directory requests. Usage is divided into intervals of NSEC "
          + "seconds.")
      .labelNames("fingerprint", "nickname", "node").register(registry);

  private static Gauge descRelayDirreqV3RespGauge = Gauge.build()
      .name("desc_relay_dirreq_v3_responses")
      . help("Estimated number of v3 network status consensus requests "
          + "that the node responded to")
      .labelNames("fingerprint", "nickname", "country").register(registry);

  private static Gauge descBridgeDirreqV3RespGauge = Gauge.build()
      .name("desc_bridge_dirreq_v3_responses")
      . help("Estimated number of v3 network status consensus requests "
          + "that the node responded to")
      .labelNames("fingerprint", "nickname", "country",
          "transport", "version").register(registry);

  private static Gauge descDirreqV3BytesGauge = Gauge.build()
      .name("desc_dirreq_v3_bytes")
      .help("Estimated number of bytes that the node wrote when answering "
          + "directory requests")
      .labelNames("fingerprint", "nickname", "node", "country",
          "transport", "version").register(registry);

  /**
   * Parse extra info descriptors and add fields to the database.
   */
  public void run(String path, Connection conn) throws Exception {
    // Read descriptors from disk.
    DescriptorReader descriptorReader =
        DescriptorSourceFactory.createDescriptorReader();

    for (Descriptor descriptor :
        descriptorReader.readDescriptors(new File(path))) {
      if ((descriptor instanceof RelayExtraInfoDescriptor)
          | (descriptor instanceof BridgeExtraInfoDescriptor)) {
        ExtraInfoDescriptor desc;
        String node = "bridge";
        if (descriptor instanceof RelayExtraInfoDescriptor) {
          desc = (RelayExtraInfoDescriptor) descriptor;
          this.isBridge = false;
          node = "relay";
        } else {
          desc = (BridgeExtraInfoDescriptor) descriptor;
          this.isBridge = true;
        }

        this.addDescriptor(desc, conn);

        String fingerprint = desc.getFingerprint();
        String nickname = desc.getNickname();
        long publishedMillis = desc.getPublishedMillis();
        SortedMap<String, Integer> responses = desc.getDirreqV3Resp();
        SortedMap<String, Integer> requests = desc.getDirreqV3Reqs();

        long dirreqStatsEndMillis = desc.getDirreqStatsEndMillis();
        long dirreqStatsIntervalLengthMillis =
            desc.getDirreqStatsIntervalLength() * 1000L;

        if (responses == null
            || publishedMillis - dirreqStatsEndMillis > ONE_WEEK_MILLIS
            || dirreqStatsIntervalLengthMillis != ONE_DAY_MILLIS) {
          /* Cut off all observations that are one week older than
           * the descriptor publication time, or we'll have to update
           * weeks of aggregate values every hour. */
          continue;
        } else {
          try {
            if (this.isBridge) {
              parseBridgeDirreqV3Resp(
                  fingerprint, nickname, publishedMillis,
                  dirreqStatsEndMillis, dirreqStatsIntervalLengthMillis,
                  responses,
                  requests,
                  desc.getBridgeIps(),
                  desc.getBridgeIpTransports(),
                  desc.getBridgeIpVersions());
              parseBridgeDirreqWriteHistory(
                  fingerprint, nickname, publishedMillis,
                  desc.getDirreqWriteHistory());
            } else {
              BandwidthHistory dirreqWriteHistory =
                  desc.getDirreqWriteHistory();
              parseRelayDirreqV3Resp(fingerprint,
                  nickname, publishedMillis, dirreqStatsEndMillis,
                  dirreqStatsIntervalLengthMillis, responses, requests);
              parseRelayDirreqWriteHistory(fingerprint,
                  nickname, publishedMillis,
                  dirreqWriteHistory);
            }
          } catch (Exception ex) {
            logger.warn("Exception. {}".format(ex.getMessage()));
          }
        }

        if (desc.getReadHistory() != null) {
          String readBandwidthHistory = desc.getReadHistory().getLine();
          this.addToMetrics(readBandwidthHistory, readBwGauge, desc, node);
        }
        if (desc.getWriteHistory() != null) {
          String writeBandwidthHistory = desc.getWriteHistory().getLine();
          this.addToMetrics(writeBandwidthHistory, writeBwGauge, desc, node);
        }
        if (desc.getIpv6ReadHistory() != null) {
          String ipv6ReadBandwidthHistory =
              desc.getIpv6ReadHistory().getLine();
          this.addToMetrics(ipv6ReadBandwidthHistory, ipv6ReadBwGauge,
              desc, node);
        }
        if (desc.getIpv6WriteHistory() != null) {
          String ipv6WriteBandwidthHistory =
              desc.getIpv6WriteHistory().getLine();
          this.addToMetrics(ipv6WriteBandwidthHistory, ipv6WriteBwGauge,
              desc, node);
        }
        if (desc.getDirreqReadHistory() != null) {
          String dirreqReadBandwidthHistory =
              desc.getDirreqReadHistory().getLine();
          this.addToMetrics(dirreqReadBandwidthHistory,
              dirreqReadBwGauge, desc, node);
        }
        if (desc.getDirreqWriteHistory() != null) {
          String dirreqWriteBandwidthHistory =
              desc.getDirreqWriteHistory().getLine();
          this.addToMetrics(dirreqWriteBandwidthHistory,
              dirreqWriteBwGauge, desc, node);
        }


      } else {
        continue;
      }
    }

    opWriter.pushToGateway(registry);

  }

  private static void parseRelayDirreqWriteHistory(String fingerprint,
      String nickname, long publishedMillis,
      BandwidthHistory dirreqWriteHistory) {
    if (dirreqWriteHistory == null
        || publishedMillis - dirreqWriteHistory.getHistoryEndMillis()
        > ONE_WEEK_MILLIS) {
      return;
      /* Cut off all observations that are one week older than
       * the descriptor publication time, or we'll have to update
       * weeks of aggregate values every hour. */
    }
    long intervalLengthMillis =
        dirreqWriteHistory.getIntervalLength() * 1000L;
    for (Map.Entry<Long, Long> e
        : dirreqWriteHistory.getBandwidthValues().entrySet()) {
      long intervalEndMillis = e.getKey();
      long intervalStartMillis =
          intervalEndMillis - intervalLengthMillis;
      for (int i = 0; i < 2; i++) {
        long fromMillis = intervalStartMillis;
        long toMillis = intervalEndMillis;
        double writtenBytes = (double) e.getValue();
        if (intervalStartMillis / ONE_DAY_MILLIS
            < intervalEndMillis / ONE_DAY_MILLIS) {
          long utcBreakMillis = (intervalEndMillis
              / ONE_DAY_MILLIS) * ONE_DAY_MILLIS;
          if (i == 0) {
            toMillis = utcBreakMillis;
          } else if (i == 1) {
            fromMillis = utcBreakMillis;
          }
          double intervalFraction = ((double) (toMillis - fromMillis))
              / ((double) intervalLengthMillis);
          writtenBytes *= intervalFraction;
        } else if (i == 1) {
          break;
        }
        opWriter.processRouterLabelmetrics(descDirreqV3BytesGauge, fingerprint,
            nickname, "relay", "", "", "", toMillis, writtenBytes);
      }
    }
  }

  private static void parseBridgeDirreqWriteHistory(String fingerprint,
      String nickname, long publishedMillis,
      BandwidthHistory dirreqWriteHistory) {

    if (dirreqWriteHistory == null
        || publishedMillis - dirreqWriteHistory.getHistoryEndMillis()
        > ONE_WEEK_MILLIS) {
      /* Cut off all observations that are one week older than
       * the descriptor publication time, or we'll have to update
       * weeks of aggregate values every hour. */
      return;
    }
    long intervalLengthMillis =
        dirreqWriteHistory.getIntervalLength() * 1000L;
    for (Map.Entry<Long, Long> e
        : dirreqWriteHistory.getBandwidthValues().entrySet()) {
      long intervalEndMillis = e.getKey();
      long intervalStartMillis =
          intervalEndMillis - intervalLengthMillis;
      for (int i = 0; i < 2; i++) {
        long fromMillis = intervalStartMillis;
        long toMillis = intervalEndMillis;
        double writtenBytes = (double) e.getValue();
        if (intervalStartMillis / ONE_DAY_MILLIS
            < intervalEndMillis / ONE_DAY_MILLIS) {
          long utcBreakMillis = (intervalEndMillis
              / ONE_DAY_MILLIS) * ONE_DAY_MILLIS;
          if (i == 0) {
            toMillis = utcBreakMillis;
          } else if (i == 1) {
            fromMillis = utcBreakMillis;
          }
          double intervalFraction = ((double) (toMillis - fromMillis))
              / ((double) intervalLengthMillis);
          writtenBytes *= intervalFraction;
        } else if (i == 1) {
          break;
        }
        opWriter.processRouterLabelmetrics(descDirreqV3BytesGauge,
            fingerprint, nickname, "bridge",
            "", "", "", toMillis, writtenBytes);
      }
    }

  }

  private void parseRelayDirreqV3Resp(String fingerprint, String nickname,
      long publishedMillis, long dirreqStatsEndMillis,
      long dirreqStatsIntervalLengthMillis,
      SortedMap<String, Integer> responses,
      SortedMap<String, Integer> requests) {
    long statsStartMillis = dirreqStatsEndMillis
        - dirreqStatsIntervalLengthMillis;
    long utcBreakMillis = (dirreqStatsEndMillis / ONE_DAY_MILLIS)
        * ONE_DAY_MILLIS;
    double resp = ((double) responses.get("ok")) - 4.0;
    if (resp > 0.0) {
      for (int i = 0; i < 2; i++) {
        long fromMillis = i == 0 ? statsStartMillis : utcBreakMillis;
        long toMillis = i == 0 ? utcBreakMillis : dirreqStatsEndMillis;
        if (fromMillis >= toMillis) {
          continue;
        }
        double intervalFraction = ((double) (toMillis - fromMillis))
            / ((double) dirreqStatsIntervalLengthMillis);
        double total = 0L;
        SortedMap<String, Double> requestsCopy = new TreeMap<>();
        if (null != requests) {
          for (Map.Entry<String, Integer> e : requests.entrySet()) {
            if (e.getValue() < 4.0) {
              continue;
            }
            double frequency = ((double) e.getValue()) - 4.0;
            requestsCopy.put(e.getKey(), frequency);
            total += frequency;
          }
        }
        /* If we're not told any requests, or at least none of them are greater
         * than 4, put in a default that we'll attribute all responses to. */
        if (requestsCopy.isEmpty()) {
          requestsCopy.put("??", 4.0);
          total = 4.0;
        }
        for (Map.Entry<String, Double> e : requestsCopy.entrySet()) {
          String country = e.getKey();
          double val = resp * intervalFraction * e.getValue() / total;
          opWriter.processRelayReqCountry(descRelayDirreqV3RespGauge,
              fingerprint, nickname, country, toMillis, val);
        }
        opWriter.processRelayReqCountry(descRelayDirreqV3RespGauge,
            fingerprint, nickname, "",
            toMillis, resp * intervalFraction);
      }
    }
  }

  private void parseBridgeDirreqV3Resp(String fingerprint, String nickname,
      long publishedMillis, long dirreqStatsEndMillis,
      long dirreqStatsIntervalLengthMillis,
      SortedMap<String, Integer> responses,
      SortedMap<String, Integer> requests,
      SortedMap<String, Integer> bridgeIps,
      SortedMap<String, Integer> bridgeIpTransports,
      SortedMap<String, Integer> bridgeIpVersions) {
    long statsStartMillis = dirreqStatsEndMillis
        - dirreqStatsIntervalLengthMillis;
    long utcBreakMillis = (dirreqStatsEndMillis / ONE_DAY_MILLIS)
        * ONE_DAY_MILLIS;
    double resp = ((double) responses.get("ok")) - 4.0;
    if (resp > 0.0) {
      for (int i = 0; i < 2; i++) {
        long fromMillis = i == 0 ? statsStartMillis
            : utcBreakMillis;
        long toMillis = i == 0 ? utcBreakMillis : dirreqStatsEndMillis;
        if (fromMillis >= toMillis) {
          continue;
        }
        double intervalFraction = ((double) (toMillis - fromMillis))
            / ((double) dirreqStatsIntervalLengthMillis);
        opWriter.processRouterLabelmetrics(descBridgeDirreqV3RespGauge,
            fingerprint, nickname, null,
            "", "", "", toMillis,
            resp * intervalFraction);
        parseBridgeRespByCategory(fingerprint,
            nickname, fromMillis, toMillis,
            resp, dirreqStatsIntervalLengthMillis, "country",
            null != requests ? requests : bridgeIps);
        parseBridgeRespByCategory(fingerprint,
            nickname, fromMillis, toMillis,
            resp, dirreqStatsIntervalLengthMillis, "transport",
            bridgeIpTransports);
        parseBridgeRespByCategory(fingerprint,
            nickname, fromMillis, toMillis,
            resp, dirreqStatsIntervalLengthMillis, "version", bridgeIpVersions);
      }
    }
  }

  private static void parseBridgeRespByCategory(String fingerprint,
      String nickname, long fromMillis, long toMillis, double resp,
      long dirreqStatsIntervalLengthMillis, String category,
      SortedMap<String, Integer> frequencies) {
    double total = 0.0;
    SortedMap<String, Double> frequenciesCopy = new TreeMap<>();
    if (frequencies != null) {
      for (Map.Entry<String, Integer> e : frequencies.entrySet()) {
        if (e.getValue() < 4.0) {
          continue;
        }
        double frequency = ((double) e.getValue()) - 4.0;
        frequenciesCopy.put(e.getKey(), frequency);
        total += frequency;
      }
    }
    /* If we're not told any frequencies, or at least none of them are
     * greater than 4, put in a default that we'll attribute all responses
     * to. */
    if (frequenciesCopy.isEmpty()) {
      switch (category) {
        case "country":
          frequenciesCopy.put("??", 4.0);
          break;
        case "transport":
          frequenciesCopy.put("<OR>", 4.0);
          break;
        case "version":
          frequenciesCopy.put("v4", 4.0);
          break;
        default:
          /* Ignore any other categories. */
      }
      total = 4.0;
    }
    for (Map.Entry<String, Double> e : frequenciesCopy.entrySet()) {
      double intervalFraction = ((double) (toMillis - fromMillis))
          / ((double) dirreqStatsIntervalLengthMillis);
      double val = resp * intervalFraction * e.getValue() / total;
      switch (category) {
        case "country":
          opWriter.processRouterLabelmetrics(
              descBridgeDirreqV3RespGauge,
              fingerprint, nickname, null, e.getKey(), "", "",
              toMillis, val);
          break;
        case "transport":
          opWriter.processRouterLabelmetrics(
              descBridgeDirreqV3RespGauge, fingerprint, nickname,
              null, "", e.getKey(), "", toMillis, val);
          break;
        case "version":
          opWriter.processRouterLabelmetrics(
              descBridgeDirreqV3RespGauge, fingerprint, nickname,
              null, "", "", e.getKey(), toMillis, val);
          break;
        default:
          /* Ignore any other categories. */
      }
    }
  }

  private void addDescriptor(ExtraInfoDescriptor desc, Connection conn) {
    DescriptorUtils descUtils = new DescriptorUtils();
    try (
      PreparedStatement preparedStatement =
          conn.prepareStatement(INSERT_EXTRA_INFO_SQL);
    ) {
      preparedStatement.setBoolean(1, this.isBridge);
      preparedStatement.setTimestamp(2,
          new Timestamp(desc.getPublishedMillis()));
      preparedStatement.setString(3, desc.getNickname());
      preparedStatement.setString(4, desc.getFingerprint());
      preparedStatement.setString(5, desc.getDigestSha1Hex());
      preparedStatement.setString(6, desc.getIdentityEd25519());
      preparedStatement.setString(7, desc.getMasterKeyEd25519());
      preparedStatement.setString(8,
          descUtils.fieldAsString(desc.getReadHistory()));
      preparedStatement.setString(9,
          descUtils.fieldAsString(desc.getWriteHistory()));
      preparedStatement.setString(10,
          descUtils.fieldAsString(desc.getIpv6ReadHistory()));
      preparedStatement.setString(11,
          descUtils.fieldAsString(desc.getIpv6WriteHistory()));
      preparedStatement.setString(12,
          desc.getGeoipDbDigestSha1Hex());
      preparedStatement.setString(13,
          desc.getGeoip6DbDigestSha1Hex());
      preparedStatement.setTimestamp(14,
          new Timestamp(desc.getGeoipStartTimeMillis()));
      preparedStatement.setString(15,
          descUtils.fieldAsString(desc.getGeoipClientOrigins()));
      if (this.isBridge == true) {
        preparedStatement.setTimestamp(16,
            new Timestamp(desc.getBridgeStatsEndMillis()));
        preparedStatement.setLong(17,
            desc.getBridgeStatsIntervalLength());
        preparedStatement.setString(18,
            descUtils.fieldAsString(desc.getBridgeIps()));
        preparedStatement.setString(19,
            descUtils.fieldAsString(desc.getBridgeIpVersions()));
        preparedStatement.setString(20,
            descUtils.fieldAsString(desc.getBridgeIpTransports()));
      } else {
        preparedStatement.setTimestamp(16,
            new Timestamp(desc.getPublishedMillis()));
        preparedStatement.setLong(17, -1L);
        preparedStatement.setString(18, "");
        preparedStatement.setString(19, "");
        preparedStatement.setString(20, "");
      }
      preparedStatement.setTimestamp(21,
          new Timestamp(desc.getDirreqStatsEndMillis()));
      preparedStatement.setLong(22,
          desc.getDirreqStatsIntervalLength());
      preparedStatement.setString(23,
          descUtils.fieldAsString(desc.getDirreqV2Ips()));
      preparedStatement.setString(24,
          descUtils.fieldAsString(desc.getDirreqV3Ips()));
      preparedStatement.setString(25,
          descUtils.fieldAsString(desc.getDirreqV2Reqs()));
      preparedStatement.setString(26,
          descUtils.fieldAsString(desc.getDirreqV3Reqs()));
      preparedStatement.setDouble(27, desc.getDirreqV2Share());
      preparedStatement.setDouble(28, desc.getDirreqV3Share());
      preparedStatement.setString(29,
          descUtils.fieldAsString(desc.getDirreqV2Resp()));
      preparedStatement.setString(30,
          descUtils.fieldAsString(desc.getDirreqV3Resp()));
      preparedStatement.setString(31,
          descUtils.fieldAsString(desc.getDirreqV2DirectDl()));
      preparedStatement.setString(32,
          descUtils.fieldAsString(desc.getDirreqV3DirectDl()));
      preparedStatement.setString(33,
          descUtils.fieldAsString(desc.getDirreqV2TunneledDl()));
      preparedStatement.setString(34,
          descUtils.fieldAsString(desc.getDirreqV3TunneledDl()));
      preparedStatement.setString(35,
          descUtils.fieldAsString(desc.getDirreqReadHistory()));
      preparedStatement.setString(36,
          descUtils.fieldAsString(desc.getDirreqWriteHistory()));
      preparedStatement.setTimestamp(37,
          new Timestamp(desc.getEntryStatsEndMillis()));
      preparedStatement.setLong(38, desc.getEntryStatsIntervalLength());
      preparedStatement.setString(39,
          descUtils.fieldAsString(desc.getEntryIps()));
      preparedStatement.setTimestamp(40,
          new Timestamp(desc.getCellStatsEndMillis()));
      preparedStatement.setLong(41, desc.getCellStatsIntervalLength());
      Array arrayCellProcessedCells =
          conn.createArrayOf("int",
          descUtils.listToArray(desc.getCellProcessedCells()));
      preparedStatement.setArray(42, arrayCellProcessedCells);
      Array arrayCellQueuedCells =
          conn.createArrayOf("float8",
          descUtils.listToArray(desc.getCellQueuedCells()));
      preparedStatement.setArray(43, arrayCellQueuedCells);
      Array arrayCellTimeInQueue =
          conn.createArrayOf("float8",
          descUtils.listToArray(desc.getCellTimeInQueue()));
      preparedStatement.setArray(44, arrayCellTimeInQueue);
      preparedStatement.setLong(45, desc.getCellCircuitsPerDecile());
      preparedStatement.setTimestamp(46,
          new Timestamp(desc.getConnBiDirectStatsEndMillis()));
      preparedStatement.setLong(47, desc.getConnBiDirectStatsIntervalLength());
      preparedStatement.setLong(48, desc.getConnBiDirectBelow());
      preparedStatement.setLong(49, desc.getConnBiDirectRead());
      preparedStatement.setLong(50, desc.getConnBiDirectWrite());
      preparedStatement.setLong(51, desc.getConnBiDirectBoth());
      preparedStatement.setTimestamp(52,
          new Timestamp(desc.getIpv6ConnBiDirectStatsEndMillis()));
      preparedStatement.setLong(53,
          desc.getIpv6ConnBiDirectStatsIntervalLength());
      preparedStatement.setLong(54, desc.getIpv6ConnBiDirectBelow());
      preparedStatement.setLong(55, desc.getIpv6ConnBiDirectRead());
      preparedStatement.setLong(56, desc.getIpv6ConnBiDirectRead());
      preparedStatement.setLong(57, desc.getIpv6ConnBiDirectBoth());
      preparedStatement.setTimestamp(58,
          new Timestamp(desc.getExitStatsEndMillis()));
      preparedStatement.setLong(59, desc.getExitStatsIntervalLength());
      preparedStatement.setString(60,
          descUtils.fieldAsString(desc.getExitKibibytesWritten()));
      preparedStatement.setString(61,
          descUtils.fieldAsString(desc.getExitKibibytesRead()));
      preparedStatement.setString(62,
          descUtils.fieldAsString(desc.getExitStreamsOpened()));
      preparedStatement.setTimestamp(63,
          new Timestamp(desc.getHidservStatsEndMillis()));
      preparedStatement.setLong(64, desc.getHidservStatsIntervalLength());
      preparedStatement.setTimestamp(65,
          new Timestamp(desc.getHidservV3StatsEndMillis()));
      preparedStatement.setLong(66, desc.getHidservV3StatsIntervalLength());
      if (desc.getHidservRendRelayedCells() != null) {
        preparedStatement.setDouble(67, desc.getHidservRendRelayedCells());
      } else {
        preparedStatement.setDouble(67, Double.NaN);
      }
      preparedStatement.setString(68,
          descUtils.fieldAsString(
          desc.getHidservRendRelayedCellsParameters()));
      if (desc.getHidservRendV3RelayedCells() != null) {
        preparedStatement.setDouble(69, desc.getHidservRendV3RelayedCells());
      } else {
        preparedStatement.setDouble(69, Double.NaN);
      }
      preparedStatement.setString(70,
          descUtils.fieldAsString(
          desc.getHidservRendV3RelayedCellsParameters()));
      if (desc.getHidservDirOnionsSeen() != null ) {
        preparedStatement.setDouble(71, desc.getHidservDirOnionsSeen());
      } else {
        preparedStatement.setDouble(71, Double.NaN);
      }
      preparedStatement.setString(72,
          descUtils.fieldAsString(desc.getHidservDirOnionsSeenParameters()));
      if (desc.getHidservDirV3OnionsSeen() != null) {
        preparedStatement.setDouble(73, desc.getHidservDirV3OnionsSeen());
      } else {
        preparedStatement.setDouble(73, Double.NaN);
      }
      preparedStatement.setString(74,
          descUtils.fieldAsString(
          desc.getHidservDirV3OnionsSeenParameters()));
      preparedStatement.setString(75,
          descUtils.fieldAsString(desc.getTransports()));
      preparedStatement.setTimestamp(76,
          new Timestamp(desc.getPaddingCountsStatsEndMillis()));
      preparedStatement.setLong(77,
          desc.getPaddingCountsStatsIntervalLength());
      preparedStatement.setString(78,
          descUtils.fieldAsString(desc.getPaddingCounts()));
      preparedStatement.setInt(79, desc.getOverloadRatelimitsVersion());
      preparedStatement.setTimestamp(80,
          new Timestamp(desc.getOverloadRatelimitsTimestamp()));
      preparedStatement.setLong(81, desc.getOverloadRatelimitsRateLimit());
      preparedStatement.setLong(82, desc.getOverloadRatelimitsBurstLimit());
      preparedStatement.setLong(83, desc.getOverloadRatelimitsReadCount());
      preparedStatement.setLong(84, desc.getOverloadRatelimitsWriteCount());
      preparedStatement.setInt(85, desc.getOverloadFdExhaustedVersion());
      preparedStatement.setTimestamp(86,
          new Timestamp(desc.getOverloadFdExhaustedTimestamp()));
      preparedStatement.setString(87, desc.getRouterSignatureEd25519());
      preparedStatement.setString(88, desc.getRouterSignature());
      preparedStatement.setString(89, "@type extra-info 1.0");
      preparedStatement.executeUpdate();

    } catch (Exception ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }
  }

  private static void addToMetrics(String bandwidthHistory,
      Gauge bwGauge, ExtraInfoDescriptor desc, String node) {
    DescriptorUtils descUtils = new DescriptorUtils();
    SimpleDateFormat dateTimeFormat = new SimpleDateFormat(
        "yyyy-MM-dd HH:mm:ss");
    String[] parts = bandwidthHistory.split(" ");
    if (parts.length != 6) {
      logger.debug("Bandwidth history line does not have expected "
          + "number of elements. Ignoring this line.");
      return;
    }
    long intervalLength;
    try {
      intervalLength = Long.parseLong(parts[3].substring(1));
    } catch (NumberFormatException e) {
      logger.debug("Bandwidth history line does not have valid interval "
          + "length '{} {}'. Ignoring this line.", parts[3], parts[4]);
      return;
    }
    String[] values = parts[5].split(",");
    if (intervalLength % 900L != 0L) {
      logger.debug("Bandwidth history line does not contain "
          + "multiples of 15-minute intervals. Ignoring this line.");
      return;
    } else if (intervalLength != 900L) {
      /* This is a really dirty hack to support bandwidth history
       * intervals that are longer than 15 minutes by linearly
       * distributing reported bytes to 15 minute intervals.  The
       * alternative would have been to modify the database schema. */
      try {
        long factor = intervalLength / 900L;
        String[] newValues = new String[values.length * (int) factor];
        for (int i = 0; i < newValues.length; i++) {
          newValues[i] = String.valueOf(
              Long.parseLong(values[i / (int) factor]) / factor);
        }
        values = newValues;
        intervalLength = 900L;
      } catch (NumberFormatException e) {
        logger.debug("Number format exception while parsing "
            + "bandwidth history line. Ignoring this line.");
        return;
      }
    }
    String type = parts[0];
    String intervalEndTime = parts[1] + " " + parts[2];
    Long intervalEnd = 0L;
    Long dateStart = 0L;
    try {
      intervalEnd = dateTimeFormat.parse(intervalEndTime).getTime();
      dateStart = dateTimeFormat.parse(parts[1] + " 00:00:00")
          .getTime();
    } catch (ParseException e) {
      logger.debug("Parse exception while parsing timestamp in "
          + "bandwidth history line. Ignoring this line.");
      return;
    }
    long published = desc.getPublishedMillis();
    if (Math.abs(published - intervalEnd)
        > 7L * 24L * 60L * 60L * 1000L) {
      logger.debug("Extra-info descriptor publication time {} and last "
          + "interval time {} in {} line differ by more than 7 days! Not "
          + "adding this line!", dateTimeFormat.format(published),
          intervalEndTime, type);
      return;
    }
    long currentIntervalEnd = intervalEnd;
    try {
      for (int i = values.length - 1; i >= -1; i--) {
        if (i == -1 || currentIntervalEnd < dateStart) {
          dateStart -= 24L * 60L * 60L * 1000L;
          intervalEndTime = dateTimeFormat.format(currentIntervalEnd);
        }
        if (i == -1) {
          break;
        }
        long bandwidthValue = Long.parseLong(values[i]);
        currentIntervalEnd -= intervalLength * 1000L;
        opWriter.processRequest(bwGauge, desc.getFingerprint(),
            desc.getNickname(), node, currentIntervalEnd, bandwidthValue);
      }
    } catch (NumberFormatException e) {
      logger.debug("Number format exception while parsing "
          + "bandwidth history line. Ignoring this line.");
      return;
    }
  }

}
