package org.torproject.metrics.descriptorparser.parsers;

import static org.junit.Assert.assertEquals;

import org.torproject.metrics.descriptorparser.utils.PsqlConnector;

import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class ConsensusParserTest {

  @Test()
  public void testConsensusParserDbUploader() throws Exception {
    ConsensusParser cp = new ConsensusParser();
    String consensusPath =
        "src/test/resources/2022-08-31-10-00-00-consensus";
    String confFile = "src/test/resources/config.properties.test";
    String networkStatusDigest = "BK9FOkQB28DWQ3KeUbV9mHi8dbsCC4gplv+zQLzmWE8";

    Connection conn = null;
    PsqlConnector psqlConn = new PsqlConnector();
    conn = psqlConn.connect(confFile);

    cp.run(consensusPath, conn);

    PreparedStatement preparedStatement = conn.prepareStatement(
        "SELECT * FROM network_status WHERE digest = '"
        + networkStatusDigest + "'");

    try (ResultSet rs = preparedStatement.executeQuery()) {
      while (rs.next()) {
        assertEquals(rs.getString("digest"), networkStatusDigest);
        assertEquals(rs.getString("consensus_flavor"), "unflavored");
      }
    }
  }
}
