package org.torproject.metrics.descriptorparser.parsers;

import org.torproject.descriptor.BridgedbMetrics;
import org.torproject.descriptor.Descriptor;
import org.torproject.descriptor.DescriptorReader;
import org.torproject.descriptor.DescriptorSourceFactory;
import org.torproject.metrics.descriptorparser.utils.DescriptorUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Map;

public class BridgedbMetricsParser {

  private static final String INSERT_BRIDGEDB_METRICS_SQL
      = "INSERT INTO"
      + " bridgedb_metrics (bridgedb_metrics_end, interval, digest, version)"
      + " VALUES (?, ?, ?, ?)";

  private static final String INSERT_BRIDGEDB_METRIC_COUNT_SQL
      = "INSERT INTO"
      + " bridgedb_metrics_count (digest, time, distribution,"
      + " transport, country, status,"
      + " tests, value, metrics)"
      + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

  private static final Logger logger = LoggerFactory.getLogger(
      BridgedbMetricsParser.class);

  /**
   * Parse brdigedb metrics and add fields to the database.
   */
  public void run(String path, Connection conn) throws Exception {
    DescriptorUtils descUtils = new DescriptorUtils();
    // Read descriptors from disk.
    DescriptorReader descriptorReader =
        DescriptorSourceFactory.createDescriptorReader();

    for (Descriptor descriptor : descriptorReader.readDescriptors(
        new File(path))) {
      if (descriptor instanceof BridgedbMetrics) {
        BridgedbMetrics desc = (BridgedbMetrics) descriptor;
        String digest = descUtils.calculateDigestSha256Base64(
            desc.getRawDescriptorBytes());

        this.addBridgedbMetrics(desc, digest, conn);

        if (desc.bridgedbMetricCounts().isPresent()) {
          for (Map.Entry<String, Long> e :
              desc.bridgedbMetricCounts().get().entrySet()) {
            String keys = e.getKey();
            Long value = e.getValue();
            this.addBridgedbMetricsCount(desc.bridgedbMetricsEnd(),
                keys, value, digest, conn);
          }
        }

      } else {
        continue;
      }
    }
  }

  private void addBridgedbMetricsCount(LocalDateTime bridgedbMetricsEnd,
      String keys, Long value, String digest, Connection conn) {
    DescriptorUtils descUtils = new DescriptorUtils();
    String[] metricsCount = keys.split(".", 5);
    String transport = metricsCount[0];
    String distribution = metricsCount[1];
    String country = metricsCount[2];
    String status = metricsCount[3];
    String tests = metricsCount[4];
    try (
      PreparedStatement preparedStatement = conn.prepareStatement(
          INSERT_BRIDGEDB_METRIC_COUNT_SQL);
    ) {
      Timestamp timestamp = Timestamp.valueOf(bridgedbMetricsEnd);
      String countDigest = descUtils.calculateDigestSha256Base64(
          keys.getBytes());
      preparedStatement.setString(1, countDigest);
      preparedStatement.setTimestamp(2, timestamp);
      preparedStatement.setString(3, distribution);
      preparedStatement.setString(4, transport);
      preparedStatement.setString(5, country);
      preparedStatement.setString(6, status);
      preparedStatement.setString(7, tests);
      preparedStatement.setLong(8, value);
      preparedStatement.setString(9, digest);
      preparedStatement.executeUpdate();
    } catch (Exception ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }
  }

  private void addBridgedbMetrics(BridgedbMetrics desc, String digest,
      Connection conn) {
    DescriptorUtils descUtils = new DescriptorUtils();

    try (
      PreparedStatement preparedStatement = conn.prepareStatement(
          INSERT_BRIDGEDB_METRICS_SQL);
    ) {
      Timestamp timestamp = Timestamp.valueOf(desc.bridgedbMetricsEnd());
      preparedStatement.setTimestamp(1, timestamp);
      preparedStatement.setLong(2,
          desc.bridgedbMetricsIntervalLength().getSeconds());
      preparedStatement.setString(3, digest);
      preparedStatement.setString(4, desc.bridgedbMetricsVersion());
      preparedStatement.executeUpdate();
    } catch (Exception ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }
  }
}
