package org.torproject.metrics.descriptorparser.parsers;

import static org.junit.Assert.assertEquals;

import org.torproject.metrics.descriptorparser.utils.PsqlConnector;

import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class ServerDescriptorParserTest {

  @Test()
  public void testServerDescriptorParserDbUploader() throws Exception {
    ServerDescriptorParser ep = new ServerDescriptorParser();
    String serverPath =
        "src/test/resources/2022-08-31-12-05-00-server-descriptors";
    String confFile = "src/test/resources/config.properties.test";
    String serverDigest = "e93044e24f71bda8595907f0429f009573e5a140";
    String fingerprint = "22F74E176F803499D4F80D9CE7D325883A8C0E45";

    Connection conn = null;
    PsqlConnector psqlConn = new PsqlConnector();
    conn = psqlConn.connect(confFile);

    ep.run(serverPath, conn);

    PreparedStatement preparedStatement = conn.prepareStatement(
        "SELECT * FROM server_descriptor WHERE digest_sha1_hex = '"
        + serverDigest + "'");

    try (ResultSet rs = preparedStatement.executeQuery()) {
      while (rs.next()) {
        assertEquals(rs.getString("digest_sha1_hex"), serverDigest);
        assertEquals(rs.getString("fingerprint"),
            fingerprint);
      }
    }

  }

}
